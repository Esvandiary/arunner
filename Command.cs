
namespace ARunner
{
    public class Command
    {
        public Command(string filename, string[] args, string wdir)
        {
            Filename = filename;
            Arguments = args;
            WorkingDirectory = wdir;
        }

        public string Filename { get; init; }
        public string[] Arguments { get; init; }
        public string WorkingDirectory { get; init; }
    }
}