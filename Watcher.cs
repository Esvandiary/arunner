using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace ARunner
{
    public class Watcher : IDisposable
    {
        public delegate bool HandlerFn(string path);

        public Watcher(string path, HandlerFn handler)
        {
            Path = path;
            Handler = handler;

            _watcher = new FileSystemWatcher(Path);
            _watcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.LastWrite | NotifyFilters.Size;
            _watcher.Filter = "*";
            _watcher.IncludeSubdirectories = false;

            _watcher.Changed += OnChanged;
            _watcher.Created += OnCreated;
            _watcher.Deleted += OnDeleted;
            _watcher.Renamed += OnRenamed;
        }

        public void Start()
        {
            DateTimeOffset now = DateTimeOffset.UtcNow;
            foreach (var path in Directory.EnumerateFileSystemEntries(Path, "*", SearchOption.TopDirectoryOnly))
                _pendingPaths[path] = now;
            _watcher.EnableRaisingEvents = true;
            WatcherThread.Instance.AddWatcher(this);
        }

        public void Stop()
        {
            WatcherThread.Instance.RemoveWatcher(this);
            _watcher.EnableRaisingEvents = false;
        }

        public void Dispose() => Dispose(true);
        protected void Dispose(bool disposing)
        {
            if (disposing)
                Stop();
        }

        public string Path { get; init; }
        public HandlerFn Handler { get; set; }

        private FileSystemWatcher _watcher;
        private ConcurrentDictionary<string, DateTimeOffset> _pendingPaths = new();

        private void NotifyThread() => WatcherThread.Instance.Notify();

        private void CheckFiles()
        {
            foreach (var path in _pendingPaths.Keys.ToArray())
            {
                try
                {
                    if (Handler(path))
                    {
                        Logging.Verbose("watcher", "handled file {0}", path);
                        _pendingPaths.TryRemove(path, out _);
                    }
                }
                catch (Exception ex)
                {
                    Logging.Warning("watcher", "exception in watcher's handler: {0}", ex.Message);
                    Logging.Debug("watcher", "exception stack trace: {0}", ex.StackTrace ?? "(none)");
                }
            }
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Changed)
                return;
            _pendingPaths[e.FullPath] = DateTimeOffset.UtcNow;
            NotifyThread();
            Logging.Verbose("watcher", "file changed: {0}", e.FullPath!);
        }

        private void OnCreated(object sender, FileSystemEventArgs e)
        {
            _pendingPaths[e.FullPath] = DateTimeOffset.UtcNow;
            NotifyThread();
            Logging.Debug("watcher", "file created: {0}", e.FullPath!);
        }

        private void OnDeleted(object sender, FileSystemEventArgs e)
        {
            var result = _pendingPaths.TryRemove(e.FullPath, out _);
            Logging.Debug("watcher", "file {0}: {1}", result ? "deleted" : "handled", e.FullPath!);
        }

        private void OnRenamed(object sender, RenamedEventArgs e)
        {
            _pendingPaths.TryRemove(e.OldFullPath, out _);
            _pendingPaths[e.FullPath] = DateTimeOffset.UtcNow;
            NotifyThread();
            Logging.Debug("watcher", "file renamed: {0}", e.FullPath!);
        }


        private class WatcherThread
        {
            public static WatcherThread Instance { get; } = new WatcherThread();

            private WatcherThread()
            {
            }

            public void AddWatcher(Watcher watcher)
            {
                lock (_lock)
                {
                    _watchers.Add(new WeakReference<Watcher>(watcher));
                    UpdateStatus();
                }
                Notify();
            }

            public void RemoveWatcher(Watcher watcher)
            {
                lock (_lock)
                {
                    _watchers.RemoveAll(t => t.TryGetTarget(out Watcher? w) && watcher == w);
                    UpdateStatus();
                }
            }

            public void Notify() => _notifyEvent.Set();

            private void Run()
            {
                Logging.Debug("watcher", "started watch thread");
                while (_running)
                {
                    _notifyEvent.WaitOne(1000);
                    if (!_running) break;
                    WeakReference<Watcher>[] watchers;
                    lock (_watchers)
                        watchers = _watchers.ToArray();
                    foreach (var weak in watchers)
                    {
                        if (weak.TryGetTarget(out Watcher? w) && w != null)
                            w.CheckFiles();
                    }
                }
                Logging.Debug("watcher", "stopped watch thread");
            }

            private void UpdateStatus()
            {
                lock (_lock)
                {
                    if (!_running && _watchers.Count != 0)
                    {
                        _running = true;
                        _thread = new Thread(Run);
                        _thread!.Start();
                    }

                    if (_running && _watchers.Count == 0)
                    {
                        _running = false;
                        Notify();
                        if (_thread!.IsAlive)
                            _thread!.Join();
                        _thread = null;
                    }
                }
            }

            private List<WeakReference<Watcher>> _watchers = new();
            private AutoResetEvent _notifyEvent = new AutoResetEvent(false);
            private Thread? _thread = null;
            private bool _running = false;
            private object _lock = new object();

        }
    }
}
