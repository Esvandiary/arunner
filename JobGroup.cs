using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ARunner
{
    public class JobGroup
    {
        public JobGroup(string name)
        {
            Name = name;
        }

        public string Name { get; init; }

        public void AddJob(Job job)
        {
            lock (_jobsLock)
            {
                _pendingJobs.Enqueue(job);
                TryStartJob();
            }
        }

        private bool TryStartJob()
        {
            lock (_jobsLock)
            {
                // don't start a job if this group already/still has one active
                if (_activeJob?.IsAlive == true)
                    return false;
                _activeJob?.Dispose();

                if (!_pendingJobs.TryDequeue(out Job? job))
                {
                    _activeJob = null;
                    return false;
                }

                Logging.Info("group", "[{0}] starting job {1:X8} for file(s) \"{2}\"",
                    Name, job!.GetHashCode(), String.Join("\", \"", job!.Inputs.Select(t => Path.GetFileName(t))));
                job!.Stopped += OnJobStopped;
                _activeJob = job;
                job!.Start();
                return true;
            }
        }

        public void Stop()
        {
            Job? active = null;
            lock (_jobsLock)
            {
                _pendingJobs.Clear();
                active = _activeJob;
            }
            active?.WaitForExit();
        }

        public void KillAll()
        {
            Job? active = null;
            lock (_jobsLock)
            {
                _pendingJobs.Clear();
                active = _activeJob;
            }
            active?.Kill();
            // OnJobStopped will TryStartJob, disposing the active job
        }

        private void OnJobStopped(object? sender, JobEventArgs e)
        {
            // assuming this is our active job, start a new one
            if (sender == _activeJob)
                TryStartJob();
        }

        private Queue<Job> _pendingJobs = new();
        private Job? _activeJob = null;
        private object _jobsLock = new Object();
    }
}