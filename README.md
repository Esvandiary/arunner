# ARunner

A simple task runner based on file watching.


### Running

The application will run until sent a SIGINT, at which point it will wait for active jobs to finish and exit.
Sending a second SIGINT will kill all active jobs and exit immediately.

Required arguments:
- `-r`/`--root-path`: the root directory under which the config, input, pending, work and output directories will exist

Optional arguments:
- `-c`/`--config-path`: overrides the directory containing `*.job.py` files defining job specs (default: `$rootpath/config`)
- `-i`/`--input-path`: overrides the input directory under which to watch for files to use (default: `$rootpath/input`)
- `-p`/`--pending-path`: overrides the pending directory under which files are stored while waiting in a queue (default: `$rootpath/pending`)
- `-w`/`--working-path`: overrides the working directory under which to have working directories for jobs (default: `$rootpath/work`)
- `-o`/`--output-path`: the root directory under which to store jobs' outputs (default: `$rootpath/output`)
- `--log-level`: the log level to output at
  - Default: `information`
  - Valid options: `fatal`, `error`, `warning`, `information`, `debug`, `verbose`


### Operation

When a file is created or changed in the relevant input folder:

- ARunner will attempt to move the file to the pending directory
- If successful, ARunner will use the job spec to generate the commands to run and the expected outputs
- The job is then added to the queue for that job group, and will start when it reaches the front
- It then runs each generated command in turn; if any command fails, the entire job is considered to have failed
- Once all commands have been run, it checks that all expected output files (if any) are present
- If they are, they are moved to the output directory and the job is considered completed; if they aren't, it has failed
- After the job ends, regardless of success or failure, the working directories for that job are cleared