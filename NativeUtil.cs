using System;
using System.Runtime.InteropServices;
using Mono.Unix;

namespace ARunner
{
    internal interface INativeUtil
    {
        public static INativeUtil Create()
        {
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                case PlatformID.WinCE:
                    return new WindowsNativeUtil();
                default:
                    return new UnixNativeUtil();
            }
        }

        public static INativeUtil Instance { get; } = Create();

        long GetFilePhysicalSize(string path);
    }

    internal class WindowsNativeUtil : INativeUtil
    {
        public WindowsNativeUtil()
        {
            Marshal.PrelinkAll(GetType());
        }

        public long GetFilePhysicalSize(string path)
        {
            UInt32 hisize = 0;
            UInt32 losize = GetCompressedFileSizeA(path, ref hisize);
            return (((long)hisize) << 32) + losize;
        }

        [DllImport("kernel32")]
        private extern static UInt32 GetCompressedFileSizeA([MarshalAs(UnmanagedType.LPStr)] string path, ref UInt32 hisize);
    }

    internal class UnixNativeUtil : INativeUtil
    {
        public UnixNativeUtil()
        {
        }

        private static int StatBlockSize = 512;

        public long GetFilePhysicalSize(string path)
        {
            return new UnixFileInfo(path).BlocksAllocated * StatBlockSize;
        }
    }
}
