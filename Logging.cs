using System;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;

namespace ARunner
{
    public class Logging : IDisposable
    {
        internal static Logging Instance { get; private set; } = new Logging();

        private const string LogFormat = "[{Timestamp:HH:mm:ss.ffffff} {HexThreadId} {Level:u1}] {Message:l}{NewLine}";

        private LoggingLevelSwitch m_levelSwitch = new LoggingLevelSwitch { MinimumLevel = LogEventLevel.Error };

        public LogEventLevel LogLevel
        {
            get { return m_levelSwitch.MinimumLevel; }
            set { m_levelSwitch.MinimumLevel = value; }
        }

        internal Logging()
        {
            RecreateLogger(true, null, false);
        }

        internal void Close()
        {
            Log.CloseAndFlush();
        }

        public void Dispose()
        {
            Close();
        }

        internal static void Fatal(string cat, string message, params object[] args) => Instance?.LogFatal(cat, message, args);
        internal static void Error(string cat, string message, params object[] args) => Instance?.LogError(cat, message, args);
        internal static void Warning(string cat, string message, params object[] args) => Instance?.LogWarning(cat, message, args);
        internal static void Info(string cat, string message, params object[] args) => Instance?.LogInfo(cat, message, args);
        internal static void Debug(string cat, string message, params object[] args) => Instance?.LogDebug(cat, message, args);
        internal static void Verbose(string cat, string message, params object[] args) => Instance?.LogVerbose(cat, message, args);

        internal void LogFatal(string cat, string message, params object[] args) => Log.Fatal("{Category,9}: {Message}", cat, String.Format(message, args));
        internal void LogError(string cat, string message, params object[] args) => Log.Error("{Category,9}: {Message}", cat, String.Format(message, args));
        internal void LogWarning(string cat, string message, params object[] args) => Log.Warning("{Category,9}: {Message}", cat, String.Format(message, args));
        internal void LogInfo(string cat, string message, params object[] args) => Log.Information("{Category,9}: {Message}", cat, String.Format(message, args));
        internal void LogDebug(string cat, string message, params object[] args) => Log.Debug("{Category,9}: {Message}", cat, String.Format(message, args));
        internal void LogVerbose(string cat, string message, params object[] args) => Log.Verbose("{Category,9}: {Message}", cat, String.Format(message, args));

        private bool RecreateLogger(bool stdout, string? filename, bool sync)
        {
            Log.CloseAndFlush();
            var lc = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(m_levelSwitch)
                .Enrich.WithHexThreadId();

            if (sync)
            {
                if (stdout)
                    lc = lc.WriteTo.Console(outputTemplate: LogFormat, theme: SystemConsoleTheme.Literate, levelSwitch: m_levelSwitch);
                if (!String.IsNullOrEmpty(filename))
                    lc = lc.WriteTo.File(filename, outputTemplate: LogFormat);
            }
            else
            {
                if (stdout)
                    lc = lc.WriteTo.Async(t => t.Console(outputTemplate: LogFormat, theme: SystemConsoleTheme.Literate, levelSwitch: m_levelSwitch));
                if (!String.IsNullOrEmpty(filename))
                    lc = lc.WriteTo.Async(t => t.File(filename, outputTemplate: LogFormat));
            }

            Log.Logger = lc.CreateLogger();
            return true;
        }
    }
}