using System;
using System.IO;
using System.Threading;
using CommandLine;
using Serilog.Events;

namespace ARunner
{
    internal class Program
    {
        internal class Options
        {
            [Option('r', "root-path", Required = true, HelpText = "Default path under which directories (config, input, pending, work, output) are located")]
            public string? RootPath { get; set; }

            [Option('c', "config-path", Required = false, HelpText = "Override for path to spec scripts")]
            public string? ConfigPath { get; set; }

            [Option('i', "input-path", Required = false, HelpText = "Override for path to use as inputs for jobs")]
            public string? InputPath { get; set; }
            [Option('p', "pending-path", Required = false, HelpText = "Override for path to use as the pending directory for jobs")]
            public string? PendingPath { get; set; }
            [Option('w', "working-path", Required = false, HelpText = "Override for path to use as the working directory for jobs")]
            public string? WorkingPath { get; set; }
            [Option('o', "output-path", Required = false, HelpText = "Override for path to use as outputs for jobs")]
            public string? OutputPath { get; set; }

            [Option("log-level", Required = false, Default = "information", HelpText = "Log level")]
            public string? LogLevel { get; set; }
        }

        public static void Main(string[] args)
        {
            Parser.Default
                .ParseArguments<Options>(args)
                .WithParsed<Options>(o =>
                {
                    if (Enum.TryParse(typeof(LogEventLevel), o.LogLevel, true, out object? level))
                        Logging.Instance.LogLevel = (LogEventLevel)level!;
                    Logging.Info("runner", "starting, working dir {0}", Environment.CurrentDirectory);

                    if (!String.IsNullOrEmpty(o.RootPath))
                    {
                        if (String.IsNullOrEmpty(o.ConfigPath))
                            o.ConfigPath = Path.Combine(o.RootPath!, "config");
                        if (String.IsNullOrEmpty(o.InputPath))
                            o.InputPath = Path.Combine(o.RootPath!, "input");
                        if (String.IsNullOrEmpty(o.PendingPath))
                            o.PendingPath = Path.Combine(o.RootPath!, "pending");
                        if (String.IsNullOrEmpty(o.WorkingPath))
                            o.WorkingPath = Path.Combine(o.RootPath!, "work");
                        if (String.IsNullOrEmpty(o.OutputPath))
                            o.OutputPath = Path.Combine(o.RootPath!, "output");
                    }

                    var paths = new SpecHandler.JobPaths
                    {
                        Input = new DirectoryInfo(o.InputPath!),
                        Pending = new DirectoryInfo(o.PendingPath!),
                        Working = new DirectoryInfo(o.WorkingPath!),
                        Output = new DirectoryInfo(o.OutputPath!)
                    };
                    paths.EnsureCreated();

                    Logging.Info("runner", "creating context");
                    var ctx = new Context();
                    ctx.Load(o.ConfigPath!, paths);
                    Logging.Info("runner", "starting context");
                    ctx.Start();

                    var evt = new ManualResetEvent(false);
                    bool alreadyStopping = false;
                    Console.CancelKeyPress += delegate(object? sender, ConsoleCancelEventArgs e) {
                        e.Cancel = true;
                        if (alreadyStopping)
                        {
                            Logging.Info("runner", "killing active jobs");
                            ctx.Kill();
                        }
                        else
                        {
                            evt.Set();
                            alreadyStopping = true;
                        }
                    };

                    evt.WaitOne();
                    Logging.Info("runner", "stopping context on signal");
                    ctx.Stop();
                    Logging.Instance.Dispose();
                });
        }
    }
}