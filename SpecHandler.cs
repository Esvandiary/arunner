using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using IronPython;
using IronPython.Hosting;
using IronPython.Runtime;
using Microsoft.Scripting.Hosting;

namespace ARunner
{
    public class SpecHandler
    {
        private delegate IList<object> ExpectedOutputsFn(string path, string working_dir);
        private delegate IList<IList<object>> CreateCommandsFn(string path, string working_dir);

        public struct JobPaths
        {
            public DirectoryInfo Input;
            public DirectoryInfo Pending;
            public DirectoryInfo Working;
            public DirectoryInfo Output;

            public void EnsureCreated()
            {
                Directory.CreateDirectory(Input.FullName);
                Directory.CreateDirectory(Pending.FullName);
                Directory.CreateDirectory(Working.FullName);
                Directory.CreateDirectory(Output.FullName);
            }
        }

        public static SpecHandler? Create(Context ctx, string jobname, JobPaths paths, string defpath)
        {
            try
            {
                var engine = Python.CreateEngine();
                engine.SetSearchPaths(new string[] {
                    // IronPython.StdLib outputs to OutDir/Lib
                    Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!, "Lib"),
                    // Enable relative imports from main defs directory
                    Path.GetDirectoryName(defpath)!
                });
                ScriptSource source = engine.CreateScriptSourceFromFile(defpath);
                ScriptScope scope = engine.CreateScope();
                source.Execute(scope);

                return new SpecHandler(ctx, jobname, paths, scope);
            }
            catch (Exception ex)
            {
                Logging.Error("spec", "failed to create spec handler for {0}: {1}", jobname, ex.Message);
                Logging.Debug("spec", "exception stack trace: {0}", ex.StackTrace ?? "(none)");
                return null;
            }
        }

        public void Start()
        {
            // check pending folder for anything left over from previous runs
            foreach (var path in Directory.EnumerateFileSystemEntries(Paths.Pending.FullName, "*", SearchOption.TopDirectoryOnly))
                CreateJob(Path.GetFullPath(path));

            _watcher.Start();
        }

        public void Stop()
        {
            _watcher.Stop();
        }

        public void Dispose() => Dispose(true);
        private void Dispose(bool disposing)
        {
            if (disposing)
                Stop();
        }

        private SpecHandler(Context ctx, string jobname, JobPaths paths, ScriptScope scope)
        {
            _context = ctx;
            Name = jobname;
            Paths = paths;
            Paths.EnsureCreated();
            Util.ClearDirectory(Paths.Working);

            _scope = scope;
            _definition = _scope.GetVariable<CreateCommandsFn>("job_get_commands");
            _expectedOutputs = _scope.GetVariable<ExpectedOutputsFn>("job_get_expected_outputs");
            string groupName = _scope.GetVariable<string>("job_group");
            Group = ctx.GetGroup(groupName);

            _watcher = new Watcher(Paths.Input.FullName, HandleFile);
            Logging.Info("spec", "created spec handler for {0} in group {1}", Name, Group.Name);
        }

        private bool HandleFile(string path)
        {
            string filename = Path.GetFileName(path);
            string winPath = Path.Combine(Paths.Pending.FullName, filename);

            try
            {
                // nasty hacks to try to figure out if file is finished copying
                FileInfo finfo = new FileInfo(path);
                // zero-length files are probably newly-created
                if (finfo.Length == 0)
                    return false;
                // sparse files are likely preallocated but not finished copying
                if (INativeUtil.Instance.GetFilePhysicalSize(path) < finfo.Length)
                    return false;
                // if something has the file locked, we shouldn't use it yet
                if (Util.IsFileLocked(finfo))
                    return false;
                // if we're happy, try to move the file
                finfo.MoveTo(winPath);
            }
            catch (Exception)
            {
                return false;
            }

            Logging.Info("spec", "[{0}] creating job in group {1} for new file \"{2}\"", Name, Group.Name, filename);
            return CreateJob(winPath);
        }

        private bool CreateJob(string path)
        {
            var job = new Job(
                GetCommands(path, Paths.Working.FullName),
                new string[] { path },
                _expectedOutputs(path, Paths.Working.FullName).Cast<string>().ToArray());
            job.Completed += OnJobCompleted;
            job.Stopped += OnJobStopped;
            Group.AddJob(job);
            return true;
        }

        private Command[] GetCommands(string filename, string wdir)
        {
            var results = _definition(filename, wdir);
            return results.Select(arr => new Command((string)arr[0], arr.Skip(1).Cast<string>().ToArray(), wdir)).ToArray();
        }

        private void OnJobCompleted(object? sender, JobEventArgs e)
        {
            Job job = (sender as Job)!;
            foreach (var output in job.ExpectedOutputs)
            {
                string filename = Path.GetFileName(output);
                string outputPath = Path.Combine(Paths.Output.FullName, filename);
                Logging.Debug("spec", "[{0}] moving output {1} to {2}", Name, filename, Paths.Output.FullName);
                try
                {
                    File.Move(output, outputPath, true);
                }
                catch (Exception ex)
                {
                    Logging.Error("spec", "[{0}] failed to move \"{1}\" to \"{2}\": {3}", Name, output, outputPath, ex.Message);
                    // don't need to do anything else as OnJobStopped will blow away anything left in the work dir
                }
            }
        }

        private void OnJobStopped(object? sender, JobEventArgs e)
        {
            Job job = (sender as Job)!;
            Logging.Debug("spec", "[{0}] clearing working directory", Name);
            Util.ClearDirectory(Paths.Working);
        }


        private Context _context;
        public string Name { get; init; }
        public JobGroup Group { get; init; }
        private ScriptScope _scope;
        private CreateCommandsFn _definition;
        private ExpectedOutputsFn _expectedOutputs;

        public JobPaths Paths { get; init; }

        private Watcher _watcher;
    }
}
