using System;
using System.Collections.Generic;
using System.IO;

namespace ARunner
{
    public class Context
    {
        public Context()
        {
            Logging.Info("context", "initialising context");
        }

        public void Load(string dirpath, SpecHandler.JobPaths rootpaths)
        {
            Logging.Info("context", "loading job specs from {0}", dirpath);
            lock (_specHandlersLock)
            {
                foreach (var path in Directory.EnumerateFileSystemEntries(dirpath, "*.job.py", SearchOption.TopDirectoryOnly))
                {
                    string file = Path.GetFileName(path);
                    string jobname = file.Remove(file.LastIndexOf(".job.py"));
                    if (_specHandlers.Remove(jobname, out SpecHandler? h))
                        h!.Dispose();

                    rootpaths.EnsureCreated();
                    var paths = new SpecHandler.JobPaths
                    {
                        Input = rootpaths.Input.CreateSubdirectory(jobname),
                        Pending = rootpaths.Pending.CreateSubdirectory(jobname),
                        Working = rootpaths.Working.CreateSubdirectory(jobname),
                        Output = rootpaths.Output.CreateSubdirectory(jobname)
                    };
                    var spec = SpecHandler.Create(this, jobname, paths, path);
                    if (spec != null)
                        _specHandlers[jobname] = spec;
                }
                Logging.Info("context", "loaded {0} job specs", _specHandlers.Count);
            }
        }

        public void Start()
        {
            lock (_specHandlersLock)
            {
                foreach (var handler in _specHandlers.Values)
                    handler.Start();
            }
        }

        public void Stop()
        {
            lock (_specHandlersLock)
            {
                foreach (var handler in _specHandlers.Values)
                    handler.Stop();
                foreach (var group in _groups.Values)
                    group.Stop();
            }
        }

        public void Kill()
        {
            lock (_groupsLock)
            {
                foreach (var group in _groups.Values)
                    group.KillAll();
            }
        }


        public JobGroup GetGroup(string name)
        {
            lock (_groupsLock)
            {
                if (!_groups.TryGetValue(name, out JobGroup? group))
                {
                    group = _groups[name] = new JobGroup(name);
                    Logging.Verbose("context", "created job group {0}", name);
                }
                return group;
            }
        }


        private Dictionary<string, SpecHandler> _specHandlers = new();
        private object _specHandlersLock = new object();
        private Dictionary<string, JobGroup> _groups = new();
        private object _groupsLock = new Object();
    }
}