using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Text;

namespace ARunner
{
    public struct LogEntry
    {
        public string Message;
        public DateTimeOffset Time;
    }

    public enum JobStatus { NotStarted, Running, Killed, Failed, Completed }

    public class JobEventArgs : EventArgs
    {
        public JobStatus Status { get; init; }
    }

    public class Job : IDisposable
    {
        public Job(Command[] commands, string[] inputs, string[] expectedOutputs)
        {
            _processes = commands.Select(c => {
                Process p = new Process();
                p.StartInfo.FileName = c.Filename;
                p.StartInfo.ArgumentList.AddRange(c.Arguments);
                p.StartInfo.WorkingDirectory = c.WorkingDirectory;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardError  = true;
                p.StartInfo.StandardOutputEncoding = Encoding.UTF8;
                p.StartInfo.StandardErrorEncoding  = Encoding.UTF8;
                p.StartInfo.UseShellExecute = false;
                return p;
            }).ToArray();

            Inputs = inputs;
            ExpectedOutputs = expectedOutputs;
        }

        public string[] Inputs;
        public string[] ExpectedOutputs;

        public bool IsAlive { get => _status == JobStatus.Running; }

        public event EventHandler<JobEventArgs>? Started;
        public event EventHandler<JobEventArgs>? Stopped;

        public event EventHandler<JobEventArgs>? Failed;
        public event EventHandler<JobEventArgs>? Completed;
        public event EventHandler<JobEventArgs>? Killed;


        private async Task MonitorStream(StreamReader reader, List<LogEntry> output)
        {
            while (_status == JobStatus.Running && reader.BaseStream.CanRead)
            {
                try
                {
                    string? line = await reader.ReadLineAsync();
                    if (line != null)
                        output.Add(new LogEntry { Message = line, Time = DateTimeOffset.UtcNow });
                    else
                        break;
                }
                catch (Exception)
                {
                    break;
                }
            }
        }

        private void OnExited(object? sender, EventArgs e)
        {
            // TODO: logging
            // if (sender != _activeProcess)
            //      eek!();

            int exitCode = _activeProcess!.ExitCode;
            Logging.Info("job", "[{0:X8}] [{1}/{2}] process {3} exited with code {4}",
                GetHashCode(), _activeProcessIndex! + 1, _processes.Length, _activeProcess!.ToInfoString(), exitCode);
            if (exitCode != 0)
            {
                OnLastProcessExited(exitCode);
                return;
            }
            while (!_activeProcess!.StandardOutput.EndOfStream)
                Thread.Yield();
            while (!_activeProcess!.StandardError.EndOfStream)
                Thread.Yield();
            _activeProcess!.StandardOutput.Close();
            _activeProcess!.StandardError.Close();
            _activeProcess!.Dispose();
            ++_activeProcessIndex;
            if (_activeProcess != null)
                StartActiveProcess();
            else
                OnLastProcessExited(exitCode);
        }

        private void OnLastProcessExited(int? exitCode)
        {
            ExitCode = exitCode;
            // if we were killed, don't claim completed/failed
            if (_status == JobStatus.Running)
            {
                if (ExitCode == 0 && ExpectedOutputs.All(t => File.Exists(t)))
                {
                    _status = JobStatus.Completed;
                    Completed?.Invoke(this, new JobEventArgs { Status = _status });
                    Logging.Info("job", "[{0:X8}] completed", GetHashCode());
                }
                else
                {
                    _status = JobStatus.Failed;
                    Failed?.Invoke(this, new JobEventArgs { Status = _status });
                    Logging.Info("job", "[{0:X8}] failed", GetHashCode());
                }

                // TODO: make this configurable?
                foreach (string path in Inputs)
                    File.Delete(path);
            }
            _stoppedEvent.Set();
            Stopped?.Invoke(this, new JobEventArgs { Status = _status });
            _activeProcess?.Dispose();
            _activeProcessIndex = null;
        }

        public void Start()
        {
            _status = JobStatus.Running;
            Logging.Info("job", "[{0:X8}] starting job", GetHashCode());
            // start first process
            _activeProcessIndex = 0;
            StartActiveProcess();
        }

        private void StartActiveProcess()
        {
            _activeProcess!.Exited += OnExited;
            _activeProcess!.EnableRaisingEvents = true;
            _activeProcess!.Start();
            _ = MonitorStream(_activeProcess!.StandardError, _stderr);
            _ = MonitorStream(_activeProcess!.StandardOutput, _stdout);
            Started?.Invoke(this, new JobEventArgs { Status = _status });
            Logging.Info("job", "[{0:X8}] [{1}/{2}] started process {1}",
                GetHashCode(), _activeProcessIndex! + 1, _processes.Length, _activeProcess!.ToInfoString());
            Logging.Debug("job", "[{0:X8}] full command: {1}",
                GetHashCode(), Util.QuoteArgumentList(new[] {_activeProcess!.StartInfo.FileName}.Concat(_activeProcess!.StartInfo.ArgumentList)));
        }

        public void Kill()
        {
            if (_status == JobStatus.Running)
            {
                _status = JobStatus.Killed;
                _activeProcess?.Kill();
                Killed?.Invoke(this, new JobEventArgs { Status = _status });
                if (_activeProcess != null)
                    Logging.Info("job", "[{0:X8}] killed, active process: {1}",  GetHashCode(), _activeProcess!.ToInfoString());
                else
                    Logging.Info("job", "[{0:X8}] killed", GetHashCode());
            }
        }

        public void WaitForExit()
        {
            if (_status == JobStatus.NotStarted || _status == JobStatus.Running)
                _stoppedEvent.WaitOne();
        }

        public int? ExitCode { get; private set; }

        public void Dispose() => Dispose(true);
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                Kill();
                _activeProcess?.WaitForExit();
            }
        }

        private Process[] _processes;
        private int? _activeProcessIndex = null;

        private Process? _activeProcess
        {
            get =>
                _activeProcessIndex.HasValue && _activeProcessIndex.Value < _processes.Length
                    ? _processes[_activeProcessIndex.Value]
                    : null;
        }

        private JobStatus _status = JobStatus.NotStarted;
        private ManualResetEvent _stoppedEvent = new ManualResetEvent(false);

        private List<LogEntry> _stdout = new(1024);
        private List<LogEntry> _stderr = new(1024);
    }
}
