using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Serilog;
using Serilog.Configuration;
using Serilog.Core;
using Serilog.Events;

namespace ARunner
{
    internal static class CollectionExtensions
    {
        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> newItems)
        {
            foreach (T item in newItems)
            {
                collection.Add(item);
            }
        }
    }

    internal static class ProcessExtensions
    {
        public static string ToInfoString(this Process process)
            => String.Format("{0} ({1})", Path.GetFileName(process.StartInfo.FileName), process.Id);
    }

    internal static class SerilogExtensions
    {
        internal static LoggerConfiguration WithHexThreadId(
            this LoggerEnrichmentConfiguration enrichmentConfiguration)
        {
            if (enrichmentConfiguration == null) throw new ArgumentNullException(nameof(enrichmentConfiguration));
            return enrichmentConfiguration.With<HexThreadIdEnricher>();
        }

        private class HexThreadIdEnricher : ILogEventEnricher
        {
            public const string ThreadIdPropertyName = "HexThreadId";

            public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
            {
                string threadId = $"{Environment.CurrentManagedThreadId:X4}";
                logEvent.AddPropertyIfAbsent(new LogEventProperty(ThreadIdPropertyName, new ScalarValue(threadId)));
            }
        }
    }
}