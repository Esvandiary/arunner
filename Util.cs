using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ARunner
{
    internal static class Util
    {
        public static void ClearDirectory(string path) => ClearDirectory(new DirectoryInfo(path));
        public static void ClearDirectory(DirectoryInfo path)
        {
            foreach (FileInfo file in path.EnumerateFiles())
                file.Delete();
            foreach (DirectoryInfo dir in path.EnumerateDirectories())
                dir.Delete(true);
        }

        public static bool IsFileLocked(string path) => IsFileLocked(new FileInfo(path));

        public static bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (var stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                    return false;
            }
            catch (IOException)
            {
                return true;
            }
        }

        public static string QuoteArgumentList(IEnumerable<string> strings)
        {
            StringBuilder sb = new();
            foreach (var s in strings)
            {
                if (s.Contains(' ') || s.Contains('\t'))
                {
                    sb.Append('"');
                    sb.Append(s);
                    sb.Append('"');
                }
                else
                {
                    sb.Append(s);
                }
                sb.Append(" ");
            }
            return sb.ToString(0, sb.Length - 1);
        }
    }
}
